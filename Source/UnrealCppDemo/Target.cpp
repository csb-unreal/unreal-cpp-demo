// Fill out your copyright notice in the Description page of Project Settings.

#include "Target.h"
#include "Bullet.h"
#include "SampleGameMode.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ATarget::ATarget()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	SetRootComponent(Mesh);

	// Bind to the event where this actor hits something. Note that AddDynamic will not autocomplete (it's a macro). Note that the function signature must be similar to what OnActorHit is asking.
	OnActorHit.AddDynamic(this, &ATarget::OnHit);

	// Initial value
	MovementRange = 200.0f;
}

// Called when the game starts or when spawned
void ATarget::BeginPlay()
{
	Super::BeginPlay();
	
	// Set a random direction
	MoveDirection = FMath::VRand();
}

void ATarget::OnHit(AActor * SelfActor, AActor * OtherActor, FVector NormalImpulse, const FHitResult & Hit)
{
	// Check if the bullet hits this actor
	if (ABullet* bullet = Cast<ABullet>(OtherActor))
	{
		// Check if we're in the correct game mode
		if (ASampleGameMode* mode = Cast<ASampleGameMode>(GetWorld()->GetAuthGameMode()))
		{
			mode->IncreasePoints();

			// Destroy target on hit
			Destroy();
		}
	}
}

// Called every frame
void ATarget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Use a sine wave to oscillate from -1 to +1
	FVector movement = MoveDirection * MovementRange * FMath::Sin(GetWorld()->GetTimeSeconds()) * DeltaTime;

	// Move. Will sweep for collision
	AddActorWorldOffset(movement, true);
}

