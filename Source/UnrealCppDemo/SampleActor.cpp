// Fill out your copyright notice in the Description page of Project Settings.

#include "SampleActor.h"
#include "Components/StaticMeshComponent.h" // We need to include every class we use here

// Sets default values
ASampleActor::ASampleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use the constructor to create components for this actor

	// Adds a static mesh component to the actor. We need to store the created component in a variable (UPROPERTY) so that we can modify its values via editor or blueprint later
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");

}

// Called when the game starts or when spawned
void ASampleActor::BeginPlay()
{
	Super::BeginPlay();

	// Set a random scale for the static mesh when the game starts
	StaticMesh->SetWorldScale3D(FMath::VRand());
}

// Called every frame
void ASampleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

